# zd234064-ref_protected

check CI_COMMIT_REF_PROTECTED

expected:

- `true` for protected branch (just `main`)
- `false` for feature branches
